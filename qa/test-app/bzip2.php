<?php

if (!function_exists('bzopen')) {
  http_response_code(500);
  echo 'BZip2 NOT enabled!' . PHP_EOL;
  return;
}

$fp = bzopen('test.txt.bz2', "r");
if ($fp === FALSE) {
  http_response_code(500);
  echo 'Bzip2 failed to open test.txt.bz2' . PHP_EOL;
  return;
}

$textFile = bzread($fp, 4096);
if (!trim($textFile) === 'Test!') {
  http_response_code(500);
  echo 'Text file is corrupt: ' . $textFile . PHP_EOL;
  return;
}
bzclose($fp);

echo 'Bzip2 works fine!' . PHP_EOL;
