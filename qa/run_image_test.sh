#!/bin/sh
set -eu

#compose_version=$(wget -qO- https://api.github.com/repos/docker/compose/releases/latest | grep 'tag_name' | cut -d\" -f4)
compose_version="1.29.2"
image_version=build_$CI_COMMIT_REF_SLUG

mkdir -p "qa/database/storage"
chown 999:999 "qa/database/storage"

docker pull "$CI_REGISTRY_IMAGE/traefik:$image_version"
docker pull "$CI_REGISTRY_IMAGE/docker-proxy:$image_version"
docker pull "$CI_REGISTRY_IMAGE/nginx:$image_version"
docker pull "$CI_REGISTRY_IMAGE/php-fpm:$image_version"
docker pull "$CI_REGISTRY_IMAGE/php-dev:$image_version"
docker pull "$CI_REGISTRY_IMAGE/mariadb:$image_version"

docker run --rm -v "$PWD":"$PWD":ro -v /var/run/docker.sock:/var/run/docker.sock:ro -e "IMAGE_VERSION=$image_version" \
  docker/compose:"$compose_version" -f "$PWD/qa/docker-compose.yml" up -d

sleep 5

docker run --rm -t -v "$PWD/qa/postman":/etc/newman --network qa_web postman/newman:5-alpine run smoke-test.json --insecure

docker run --rm -v "$PWD":"$PWD":ro -v /var/run/docker.sock:/var/run/docker.sock:ro -e "IMAGE_VERSION=$image_version" \
  docker/compose:"$compose_version" -f "$PWD/qa/docker-compose.yml" -f "$PWD/qa/docker-compose.dev.yml" up -d

sleep 5

docker run --rm -t -v "$PWD/qa/postman":/etc/newman --network qa_web postman/newman:5-alpine run smoke-test.json --insecure
